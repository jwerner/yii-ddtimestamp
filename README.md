DDTimestamp Extension
=====================

This extension provides logging functionality to Yii CActiveRecords.

Instead of creating _created_ and _changed_ columns in every database table,  
this extension uses just one table for all log infos.

Installation
------------

Extract the files in your _protected/extensions_ directory.

Create one database table for all ActiveRecord log infos:

    CREATE TABLE `tbl_artimestamp` (
      `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
      `ownerName` varchar(255) NOT NULL COMMENT 'Class name of owner ActiveRecord',
      `ownerId` int(11) NOT NULL,
      `userIdCreated` int(11) NOT NULL,
      `userNameCreated` varchar(255) NOT NULL,
      `tsCreated` int(11) NOT NULL,
      `userIdUpdated` int(11) DEFAULT NULL,
      `userNameUpdated` varchar(255) DEFAULT NULL,
      `tsUpdated` int(11) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `ownerName_ownerId` (`ownerName`,`ownerId`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ActiveRecord Create/Update Timestamps';


Usage
-----

### Model Behavior

Add the behavior to your model's behaviors:

    public function behaviors()
    {
        return array(
            // ...
            'DDTimestampBehavior' => array(
                'class'                 => 'ext.diggindata.ddtimestamp.behaviors.DDTimestampBehavior',
                'timestampTableName'    => '{{artimestamp}}',
                'userClassName'         => 'User',
                'userNameAttribute'     => 'recordName',
                'setUpdateOnCreate'     => true,
            )
            // ...
        ); 
    }

#### Behavior Settings

* **timestampTableName**:  
  table name for log infos (defaults to _{{artimestamp}}_, may include table prefix braces
* **userClassName**:  
  Name of web user CActiveRecord class (defaults to _User_)
* **userNameAttribute**:  
  User's name column (defaults to _username_, can also be a virtual attribute)
* **setUpdateOnCreate**:  
  if _true_, sets update timestamp and username also on inserting a new record

### Widget

To display the log infos in a record's detail view, add the following code  
in a _views/model/view.php_ file:

    <?php $this->widget('ext.diggindata.ddtimestamp.widgets.DDTimestamp', array(
        'model'=>$model, 
        'title'=>Yii::t('site','Change Log'),
        'detailViewWidget'=>'bootstrap.widgets.TbDetailView'
    )); ?>

#### Widget Settings

* **model**:  
  Instance of CActiveRecord model
* **title**:  
  Title to be displayed above the widget (defaults to 'Change Log')
* **style**:  
  Either _small_ or not specified (_small_ displays a textual presentation, not specified shows a DetailView)
* **detailViewWidget**:  
  Yii path alias to DetailView widget (defaults to _zii.widgets.CDetailView_, could also be _bootstrap.widgets.TbDetailView_)


(C) 2013 Joachim Werner <joachim.werner@diggin-data.de>
