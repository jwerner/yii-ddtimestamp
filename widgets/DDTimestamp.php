<?php
/**
 * DDTimestamp class file
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */

/**
 * DDTimestamp displays the record's change log in a DetailView.
 *
 * Usage:
 * <?php $this->widget('ext.diggindata.ddtimestamp.widgets.DDTimestamp', array(
 *     'model'=>$model,
 *     'title'=>Yii::t('category','Change Log'),
 *     'style'=>'small', // 'small' for textual presentation, leave to get a DetailView
 *     'detailViewWidget'=>'bootstrap.widgets.TbDetailView' // or zii.widgets.CDetailView
 * )); ?>
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */
class DDTimestamp extends CWidget
{
    // {{{ *** Members ***
    /**
     * @var string Title to be displayed above timestamp detail view
     * @access public
     */
    public $title = 'Change Log';
    /**
     * @var MyModel ActiveRecord instance
     */
    public $model;
    /**
     * @var string If 'small', then small template is rendered
     * @access public
     */
    public $style;
    /**
     * @var string Alias to DetailView base widget
     *
     * E.g. 'zii.widgets.CDetailView' or 'bootstrap.widgets.TbDetailView'
     */
    public $detailViewWidget = 'zii.widgets.CDetailView';
    /**
     * @var array of DetailView options
     */
    public $options=array();
    /**
     * @var array of HTML options for the DetailView
     */
    public $htmlOptions=array();
    // }}} 
    // {{{ *** Methods ***
    // {{{ init
    public function init()
    {
	    $this->htmlOptions['id']=$this->getId();

    } // }}} 
    // {{{ run
    public function run()
    {
        $timestamps = $this->model->getTimestamps();
        $template = 'timestampWidget';
        if($this->style=='small')
            $template .= 'Small';
        $this->render($template,array(
            'title'=>$this->title,
            'detailViewWidget'=>$this->detailViewWidget,
            'data'=>$timestamps, 
            'options'=>$this->options, 
            'htmlOptions'=>$this->htmlOptions
        ));
    } // }}} 
    // }}} End Methods
}
/* vim:set ai sw=4 sts=4 et fdm=marker fdc=4: */ 
