<div align="right">
<?php echo CHtml::encode(Yii::t('site','Created On {dateCreated} by {userCreated}',array('{dateCreated}'=>Yii::app()->dateFormatter->formatDateTime($data['tsCreated'],'medium','short'),'{userCreated}'=>$data['userNameCreated']))); ?><br/>
<?php echo CHtml::encode(Yii::t('site','Last Changed On {dateUpdated} by {userUpdated}',array('{dateUpdated}'=>Yii::app()->dateFormatter->formatDateTime($data['tsUpdated'],'medium','short'),'{userUpdated}'=>$data['userNameUpdated']))); ?>
</div>
