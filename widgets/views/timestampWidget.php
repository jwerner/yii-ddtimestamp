<h3><?php echo CHtml::encode($title); ?></h3>
<?php if($data==false) : ?>
<p><?php echo CHtml::encode(Yii::t('site','There is no change log available for this item.')); ?>
<?php else : ?>
</p>
<?php
$properties = array(
    'data'=>$data,
    'attributes' => array(
        array(
            'label' => Yii::t('site','Created On'),
            'type' => 'raw',
            'value' => Yii::app()->dateFormatter->formatDateTime($data['tsCreated'],'medium','long')
        ),
        array(
            'label' => Yii::t('site','Created By'),
            'type' => 'raw',
            'value' => $data['userNameCreated']
        ),
        array(
            'label' => Yii::t('site','Updated On'),
            'type' => 'raw',
            'value' => Yii::app()->dateFormatter->formatDateTime($data['tsUpdated'],'medium','long')
        ),
        array(
            'label' => Yii::t('site','Updated By'),
            'type' => 'raw',
            'value' => $data['userNameUpdated']
        ),
    )
);
$properties = CMap::mergeArray($properties, $options);
$this->widget($detailViewWidget, $properties); ?>
<?php endif; ?>
