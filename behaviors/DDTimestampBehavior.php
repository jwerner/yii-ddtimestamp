<?php
/**
 * DDTimestampBehavior class file.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */

 /**
 * DDTimestampBehavior will automatically fill date and time related atributes.
 *
 * DDTimestampBehavior will automatically fill date and time related atributes when the active record
 * is created and/or upadated.
 * You may specify an active record model to use this behavior like so:
 * <pre>
 * public function behaviors(){
 *     return array(
 *         'DDTimestampBehavior' => array(
 *             'class'              => 'ext.diggindata.DDTimestamp2Behavior',
 *             'timestampTableName' => '{{artimestamp}}',
 *             'userNameAttribute'  => 'recordName',
 *             'setUpdateOnCreate'  => true // set updated timestamp and user name also on inserting a new record
 *         )
 *     );
 * }
 * </pre>
 * The {@link tsCreateAttribute} and {@link tsUpdateAttribute} options actually default to 'tsCreated' and 'tsUpdated'
 * respectively, so it is not required that you configure them.  If you do not wish TimestampBehavior
 * to set a timestamp for record update or creation, set the corrisponding attribute option to null.
 *
 * By default, the update attribute is only set on record update.  If you also wish it to be set on record creation,
 * set the {@link setUpdateOnCreate} option to true.
 *
 * Although TimestampBehavior attempts to figure out on it's own what value to inject into the timestamp attribute,
 * you may specify a custom value to use instead via {@link timestampExpression}
 *
 * @author Jonah Turnquist <poppitypop@gmail.com>
 * @author Joachim Werner <joachim.pt.werner@de.gm.com>
 * @version $Id: CTimestampBehavior.php 2934 2011-02-02 21:34:43Z qiang.xue $
 * @package zii.behaviors
 * @since 1.1
 */

class DDTimestampBehavior extends CActiveRecordBehavior 
{
    // {{{ *** Members ***

    public $timestampTableName = '{{artimestamp}}';

    public $userClassName = 'User';

    public $userNameAttribute = 'username';

    /**
    * @var bool Whether to set the update attribute to the creation timestamp upon creation.
    * Otherwise it will be left alone.  Defaults to false.
    */
    public $setUpdateOnCreate = false;

    // }}} 
    // {{{ afterSave
    /**
     * Responds to {@link CModel::onAfterSave} event.
     * Sets the values of the creation or modified attributes as configured
     *
     * @param CModelEvent $event event parameter
     */
    public function afterSave($event) 
    {
        $command = Yii::app()->db->createCommand();
        // Get user model
        $className = $this->userClassName;
        $user = new $className;
        $user = $user->findByPk(Yii::app()->user->id);
        // New record
        if ( $this->getOwner()->getIsNewRecord() ) {
            $attributes = array(
                'ownerName'         =>get_class($this->getOwner()),
                'ownerId'           =>$this->getOwner()->primaryKey,
                'userIdCreated'     =>$user->id,
                'userNameCreated'   =>$user->{$this->userNameAttribute},
                'tsCreated'         => time()
            );
            if ( $this->setUpdateOnCreate==true ) {
                $attributes['userIdUpdated']    = $user->id;
                $attributes['userNameUpdated']  = $user->{$this->userNameAttribute};
                $attributes['tsUpdated']        = time();
            }
            $result = $command->insert( $this->timestampTableName, $attributes);
            Yii::log('Rows affected (Insert): '.$result, 'info', 'DDTimestampBehavior' );
            
        } else {
            $attributes = array(
                'userIdUpdated'     => $user->id,
                'userNameUpdated'   => $user->{$this->userNameAttribute},
                'tsUpdated'         => time()
            );
            $result = $command->update( 
                $this->timestampTableName, 
                $attributes, 
                'ownerName=:ownerName AND ownerId=:ownerId', 
                array(
                    ':ownerName'    => get_class($this->getOwner()),
                    ':ownerId'      => $this->getOwner()->primaryKey
                )
            );
            Yii::log('Rows affected (Update): '.result, 'info', 'DDTimestampBehavior' );
        }
    } // }}} 
    public function afterDelete()
    {
        $command = Yii::app()->db->createCommand();
        $command->delete($this->timestampTableName, ':ownerName=:ownerName AND ownerId=:ownerId', array(
            ':ownerName'    => get_class($this->getOwner()),
            ':ownerId'      => $this->getOwner()->primaryKey
        ));
    }
    // {{{ getTimestamps
    /**
     * Returns an associative array of timestamp attributes
     *
     * @return mixed
     */
    public function getTimestamps()
    {
        $command = Yii::app()->db->createCommand();
        $command->select();
        $command->from( $this->timestampTableName);
        $command->where('ownerName=:ownerName AND ownerId=:ownerId');
        $result = $command->queryRow( true, array(
            ':ownerName'    => get_class($this->getOwner()),
            ':ownerId'      => $this->getOwner()->primaryKey
        ));
        return $result;
    } // }}} 
}
/* vim:set ai sw=4 sts=4 et fdm=marker fdc=4: */ 
?>
